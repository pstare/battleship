require 'battleship/errors'

module Battleship
  class Ship
    attr_accessor :position

    def initialize
      @position = []
    end

    def build(x, y)
      position << { x: x, y: y, status: :undamaged }
    end

    def coordinate_at(x, y)
      position.find { |p| p[:x] == x && p[:y] == y }
    end

    def status_at(x, y)
      coordinate_at(x, y)[:status]
    end

    def hit(x, y)
      coordinate_at(x, y)[:status] = :hit
    end

    def sunk?
      position.all? { |coordinate| coordinate[:status] == :hit }
    end
  end
end
