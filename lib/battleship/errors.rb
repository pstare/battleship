module Battleship
  class BoardParseError < StandardError
  end

  class GameOver < StandardError
  end
end
