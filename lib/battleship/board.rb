require 'battleship/ship'
require 'battleship/errors'

module Battleship
  class Board
    attr_reader   :boardspec
    attr_accessor :field, :ships

    def initialize(boardspec:)
      @boardspec = boardspec

      parse_board
    end

    def parse_board
      @field = []
      @ships = {}

      lines = boardspec.split("\n").reverse

      lines.each.with_index do |line, y|
        columns = line.split('')
        columns.each.with_index do |coordinate, x|
          type = convert(coordinate.upcase)
          field[y] ||= []
          field[y][x] = type

          if type
            ships[type] ||= Ship.new
            ships[type].build(x,y)
          end
        end
      end
    end

    def convert(type)
      case type
      when 'C'
        :carrier
      when 'B'
        :battleship
      when 'R'
        :cruiser
      when 'S'
        :submarine
      when 'D'
        :destroyer
      when '0'
        nil
      else
        raise BoardParseError, "You suck"
      end
    end

    def check(x, y)
      field[y][x]
    end

    def fire_on(x, y)
      ship   = ships[check(x, y)]
      result = ship ? ship.hit(x, y) : :miss

      if result == :hit && game_over?
        raise GameOver, "All ships on this board have been sunk!"
      end

      result
    end

    def game_over?
      ships.all? { |_, ship| ship.sunk? }
    end
  end
end
