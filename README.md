# Battleship

Shopify fifth interview pairing exercise. Implement as much of a battleship
simulator as possible, in Ruby.

## Installation

From the project directory:

```text
bundle install
```

## Running tests

```text
bundle exec rake test
```

## Playing around

To play around in the console:

```text
bundle exec pry
```
