require 'test_helper'
require 'awesome_print'
require 'battleship/board'

describe Battleship::Board do
  subject { Battleship::Board.new boardspec: specification }
  let(:specification) do
    <<~SPECIFICATION
      0000000000
      00C00DD000
      00C0000S00
      00C0000S00
      00C0000S00
      00C0000000
      00000RRR00
      0000000000
      00BBBB0000
      0000000000
    SPECIFICATION
  end

  describe '#field' do
    it 'returns a valid field' do
      assert subject.field
    end
  end

  describe '#check' do
    describe 'when checking for a battleship hit' do
      let(:x) { 3 }
      let(:y) { 1 }

      it 'finds one' do
        assert_equal :battleship, subject.check(x, y)
      end
    end

    describe 'when checking for a known miss' do
      let(:x) { 1 }
      let(:y) { 1 }

      it 'finds no hit' do
        assert_nil subject.check(x, y)
      end
    end
  end

  describe "when checking #ships for the battleship's position" do
    let(:expected_coordinates) do
      [
        { x: 2, y: 1, status: :undamaged },
        { x: 3, y: 1, status: :undamaged },
        { x: 4, y: 1, status: :undamaged },
        { x: 5, y: 1, status: :undamaged },
      ]
    end

    it 'finds all the correct coordinates with status undamaged' do
      expected_coordinates.each do |expected_coordinate|
        assert_includes subject.ships[:battleship].position, expected_coordinate
      end
    end
  end

  describe "when firing the first shot on a ship" do
    let(:x)         { 3 }
    let(:y)         { 1 }
    let(:ship_type) { subject.check(x, y) }
    let(:ship)      { subject.ships[ship_type] }

    before do
      refute_nil ship
      ship.hit(x, y)
    end

    it "the ship's status shows the correct positional damage" do
      assert_equal :hit, ship.status_at(x, y)
    end

    it "the ship is not yet sunk" do
      refute ship.sunk?
    end

    describe "after firing on the remaining undamaged coordinates" do
      let(:remaining) do
        ship.position.select { |coordinate| coordinate[:status] == :undamaged }
      end

      before do
        remaining.each { |r| ship.hit(r[:x], r[:y]) }
      end

      it "the ship is sunk" do
        assert ship.sunk?
      end
    end
  end

  describe "#fire_on" do
    describe "when firing on a known battleship coordinate" do
      let(:x) { 3 }
      let(:y) { 1 }

      it "returns :hit" do
        assert_equal :hit, subject.fire_on(x, y)
      end
    end

    describe "when firing on a known empty coordinate" do
      let(:x) { 4 }
      let(:y) { 6 }

      it "returns :miss" do
        assert_equal :miss, subject.fire_on(x, y)
      end
    end

    describe "when all ships have been completely destroyed" do
      it "ends the game with a GameOver exception" do
        assert_raises Battleship::GameOver do
          subject.ships.each do |_, ship|
            ship.position.each { |c| subject.fire_on(c[:x], c[:y]) }
          end
        end
      end
    end
  end
end
