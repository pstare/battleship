$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'battleship'

require 'minitest/autorun'
require 'minitest/reporters'
require 'pry'

Minitest::Reporters.use! [Minitest::Reporters::DefaultReporter.new(:color => true)]
